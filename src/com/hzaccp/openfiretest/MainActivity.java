package com.hzaccp.openfiretest;

import org.jivesoftware.smack.XMPPConnection;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hzaccp.openfiretest.biz.SE;
import com.hzaccp.openfiretest.util.ExitUtil;

/**
 * 登录界面
 * */
public class MainActivity extends Activity {
	EditText txtUserName;
	EditText txtPassword;
	EditText txtIP;
	EditText txtPort;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ExitUtil.getInstance().addActivity(this);//添加退出
		setContentView(R.layout.activity_main);
		init();
	}

	private void init() {
		Button btnLogin = (Button) findViewById(R.id.btnLogin);
		txtUserName = (EditText) findViewById(R.id.txtUserName);
		txtPassword = (EditText) findViewById(R.id.txtPassword);
		txtIP = (EditText) findViewById(R.id.txtIP);
		txtPort = (EditText) findViewById(R.id.txtPort);

		//登录操作
		btnLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					String a = txtIP.getText().toString();
					int b = Integer.parseInt(txtPort.getText().toString());
					XMPPConnection con = SE.getInstance().getCon(a, b);
					con.login(txtUserName.getText().toString(), txtPassword.getText().toString(), "OPENFIRETEST");//第三个参数:客户端名称,传文件时用到

					if (con.isAuthenticated()) {//登录成功
						Intent intent = new Intent(MainActivity.this, UsersActivity.class);
						startActivity(intent);
						MainActivity.this.finish();
					} else {
						Toast.makeText(MainActivity.this, "登录失败", Toast.LENGTH_LONG).show();
					}
				} catch (Exception err) {
					err.printStackTrace();
				}
			}
		});
	}

}
