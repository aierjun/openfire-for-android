最近在做android项目，其中有个IM模块。后来服务器选用了openfire，手机上用的是aSmack。实现文字聊天、传图片及传文件。<br />
关于为什么选用openfire及Openfire其它相关的信息，这里就不描述了，网上很多。<br />
<br />
<strong>需要准备资源:</strong><br />
<span style="white-space:pre">Openfire服务器，在可以<a target="_blank" href="http://www.igniterealtime.org/">http://www.igniterealtime.org/</a>下载.</span><br />
<span style="white-space:pre">aSmack.jar 在我的资源包中有，也可以去<a target="_blank" href="https://code.google.com/p/asmack/">https://code.google.com/p/asmack/</a>下载.</span><br />
<span style="white-space:pre">Spark 这个可以不下载，我用于测试，在<a target="_blank" href="http://www.igniterealtime.org/">http://www.igniterealtime.org/</a>下载.</span><br />
<br />
<strong>我的源码:&nbsp;</strong><br />
<span style="white-space:pre"><a target="_blank" href="http://git.oschina.net/hzaccp/openfire-for-android">http://git.oschina.net/hzaccp/openfire-for-android</a></span><br />
<br />
<strong>类结构:</strong><br />
<span style="white-space:pre"><img src="http://img.blog.csdn.net/20130815234912187?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvaHphY2NwMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast" alt="" /></span><br />
<br />
CO.java：openfirer操作类，包括信息监听及文件监听。<br />
DB.java：为信息保存工具类，保存、读取聊天信息。<br />
SE.java：客户端会话类，类似web服务器上的session，保存登录用户的openfire连接、当前与谁在会话及客户端当前是哪个活动界面。<br />
MainActivity.java：登录界面<br />
UsersActivity.java：登录后的用户列表界面<br />
ChatActivity.java：聊天界面<br />
<br />
<strong>流程:</strong><br />
<span style="white-space:pre"><img src="http://img.blog.csdn.net/20130815235126937?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvaHphY2NwMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast" alt="" /></span><br />
流程主要分两条，一条为主线程，用于显示，另一条为后台线程，用于后台监听信息。后台接收到信息后，通过SE这个工具类，显示到界面中。<br />
<br />
